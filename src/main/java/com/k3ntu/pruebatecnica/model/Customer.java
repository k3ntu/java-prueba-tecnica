package com.k3ntu.pruebatecnica.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "customers")
@Getter
@Setter
@EqualsAndHashCode(of = { "id" }, doNotUseGetters = true, callSuper = false)
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "cellphone", nullable = false)
    private String cellphone;

    public Customer() {
    }

    public Customer(Long id) {
        this.id = id;
    }

    public Customer(Long id, String name, String lastname, String address, String email, String cellphone) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.address = address;
        this.email = email;
        this.cellphone = cellphone;
    }

    public Customer(String name, String lastname, String address, String email, String cellphone) {
        this.name = name;
        this.lastname = lastname;
        this.address = address;
        this.email = email;
        this.cellphone = cellphone;
    }
}
