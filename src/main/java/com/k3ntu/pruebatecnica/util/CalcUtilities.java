package com.k3ntu.pruebatecnica.util;

public class CalcUtilities {

    public static boolean checkNumberPrime(int n) {
        int i;
        int m=0;
        int flag=0;

        m=n/2;
        if(n==0||n==1){
            return false;
        }else{
            for(i=2;i<=m;i++){
                if(n%i==0){
                    flag=1;
                    break;
                }
            }
            return flag == 0;
        }
    }

}
