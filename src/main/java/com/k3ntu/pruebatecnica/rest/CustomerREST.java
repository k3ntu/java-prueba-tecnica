package com.k3ntu.pruebatecnica.rest;

import com.k3ntu.pruebatecnica.dto.InfoCalcPrimeNumberDto;
import com.k3ntu.pruebatecnica.model.Customer;
import com.k3ntu.pruebatecnica.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/customer")
public class CustomerREST {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/")
    private ResponseEntity<List<Customer>> customerAll () {
        return ResponseEntity.ok(customerService.getAllCustomers());
    }

    @GetMapping("/{id}")
    private ResponseEntity<Optional<Customer>> customerFind (@PathVariable ("id") Long id) {
        return ResponseEntity.ok(customerService.getCustomerById(id));
    }

    @GetMapping("/calcNumberPrime")
    private ResponseEntity<InfoCalcPrimeNumberDto> calcCustomerNumberPrime () {
        return ResponseEntity.ok(customerService.calcPrimeNumber());
    }

    @PostMapping("/add")
    private ResponseEntity<Customer> save(@RequestBody Customer customer) {
        Customer temp = customerService.create(customer);


        try {
            return ResponseEntity.created(new URI("/api/customer/" + temp.getId())).body(temp);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PutMapping("/update")
    private ResponseEntity<Customer> customerUpdate (@RequestBody Customer customer) {
        Customer temp = customerService.create(customer);

        try {
            return ResponseEntity.created(new URI("/api/customer" + temp.getId())).body(temp);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @DeleteMapping
    private ResponseEntity<Void> customerDelete (@RequestBody Customer customer) {
        customerService.delete(customer);

        return ResponseEntity.ok().build();
    }


}
