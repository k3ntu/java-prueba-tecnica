package com.k3ntu.pruebatecnica.service;

import com.k3ntu.pruebatecnica.dto.InfoCalcPrimeNumberDto;
import com.k3ntu.pruebatecnica.model.Customer;
import com.k3ntu.pruebatecnica.repository.CustomerRepository;
import com.k3ntu.pruebatecnica.util.CalcUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer create(Customer customer) {
        return customerRepository.save(customer);
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Optional<Customer> getCustomerById(Long id) {
        return customerRepository.findById(id);
    }

    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }

    public InfoCalcPrimeNumberDto calcPrimeNumber() {
        InfoCalcPrimeNumberDto result = new InfoCalcPrimeNumberDto();
        List<Customer> customers = this.getAllCustomers();

        if (!customers.isEmpty()) {
            customers = getCustomersNameNumberPrime(customers);

            result.setQuantityNumberPrime(customers.size());
            result.setNamesPrime(customers.stream().map(Customer::getName).collect(Collectors.toList()));
        }

        return result;
    }

    private List<Customer> getCustomersNameNumberPrime(List<Customer> customers) {
        return customers
                .stream()
                .filter(customer -> CalcUtilities.checkNumberPrime(customer.getName().length()))
                .collect(Collectors.toList());
    }



}
