package com.k3ntu.pruebatecnica.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InfoCalcPrimeNumberDto {

    private int quantityNumberPrime;

    private List<String> namesPrime;

    public InfoCalcPrimeNumberDto() {
        quantityNumberPrime = 0;
        namesPrime = new ArrayList<>();
    }
}
