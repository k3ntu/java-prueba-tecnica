package com.k3ntu.pruebatecnica.repository;

import com.k3ntu.pruebatecnica.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {



}
